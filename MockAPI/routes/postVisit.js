const router = require('express').Router();
var dateFormat = require('dateformat');
const fs = require('fs');

router.get('/visitCode/:code?', async (req, res) => {
    responsecode['visit'] = req.params.code||200;    
    res.status(200).send({success:true,currentcode:responsecode['visit']});
    return true;
});

router.post('/visit/', async (req, res) => {
    let rescode = responsecode['visit'];
    let responseData = {rescode:rescode};
    
    if (rescode == 200){ //Fixed 200 With Condition(200/403)        
        if(req.body.p_run_hn && req.body.p_year_hn && req.body.p_opd && req.body.p_user_created){ //case 200
            let opdclinic = (await pool.query('SELECT * FROM tb_opdclinic WHERE clinic_code = $1', [req.body.p_opd])).rows;
            let patient = (await pool.query(`SELECT * FROM tr_patient WHERE hn = '${req.body.p_run_hn}/${req.body.p_year_hn}'`)).rows;
            if( opdclinic[0] && patient[0] ){ // Found Clinic
                responseData = {
                    "pid": patient[0]['idcard'],
                    "hn": patient[0]['hn'],
                    "parentRoomId": opdclinic[0]['clinic_code'],
                    "parentRoomName": opdclinic[0]['detail'],
                    "childRoomId": opdclinic[0]['clinic_code'],
                    "childRoomName": opdclinic[0]['detail'],
                    "visitNo": 28334548
                };
            }else{ // Not Found Clinic                
                rescode = 403;
                responseData = {
                    "ERROR-MESSAGe": "ORA-20999: -20010,ORA-20010: ไม่พบ HN หรือ รหัส subspecialty ดังกล่าว\nORA-06512: at \"KAMOLJIT.PMKHMS\", line 889\nORA-06512: at \"KAMOLJIT.PREINSUPD_OPD\", line 820\nORA-04088: error during execution of trigger 'KAMOLJIT.PREINSUPD_OPD'\nORA-06512: at \"KAMOLJIT.PMKHMS\", line 889\nORA-06512: at \"KAMOLJIT.F_VISIT\", line 219",
                    "status": "error"
                };
            }
        }else{ //case 403
            rescode = 403
            if(!req.body.p_run_hn && !req.body.p_year_hn){ //No Full HN                
                responseData = {
                    "ERROR-MESSAGe": "ORA-20999: -20999,ORA-20999: ต้องระบุ HN ในการเปิดสิทธิ\nORA-06512: at \"KAMOLJIT.PMKHMS\", line 889\nORA-06512: at \"KAMOLJIT.F_VISIT\", line 219",
                    "status": "error"
                };
            }else if(!req.body.p_run_hn || !req.body.p_year_hn){ //No HN || Year                
                responseData = {
                    "ERROR-MESSAGe": "ORA-20999: -20999,ORA-20999: HN ต้องระบุข้อมูลให้ครบ\nORA-06512: at \"KAMOLJIT.PMKHMS\", line 889\nORA-06512: at \"KAMOLJIT.F_VISIT\", line 219",
                    "status": "error"
                };
            }else if(!req.body.p_opd){ //No OPDCode                
                responseData = {
                    "ERROR-MESSAGe": "ORA-20999: -20999,ORA-20999: ต้องระบุรหัสห้องตรวจ จึงจะเปิด Visit ได้\nORA-06512: at \"KAMOLJIT.PMKHMS\", line 889\nORA-06512: at \"KAMOLJIT.F_VISIT\", line 219",
                    "status": "error"
                };
            }else if(!req.body.p_user_created){ //No User                
                responseData = {
                    "ERROR-MESSAGe": "ORA-20999: -20999,ORA-20999: ต้องระบุ user ที่เกี่ยวข้องมาด้วย\nORA-06512: at \"KAMOLJIT.PMKHMS\", line 889\nORA-06512: at \"KAMOLJIT.F_VISIT\", line 219",
                    "status": "error"
                }
            }
        }
    }else if ( rescode == 403 ){ //Fixed 403 Duplicate Case        
        rescode = 403;
        responseData = {
            "ERROR-MESSAGe": "ORA-20999: -20999,ORA-20999: ระบบมีการส่งรายชื่อของท่านไปยังห้องตรวจ OPD.MED ในวันนี้แล้ว\nORA-06512: at \"KAMOLJIT.PMKHMS\", line 889\nORA-06512: at \"KAMOLJIT.F_VISIT\", line 219",
            "status": "error"
        };
    }
    res.status(rescode).send(responseData);
    return true;
});

module.exports = router;