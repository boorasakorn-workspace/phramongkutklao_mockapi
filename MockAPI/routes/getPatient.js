const router = require('express').Router();
var dateFormat = require('dateformat');
const fs = require('fs');

router.get('/patient/:year?/:hn?/:pid?', async (req, res) => {
    //if ( req.params.pid|| (req.params.year && req.params.hn)) {
    if ( (req.params.pid && req.params.pid != 'null') || (req.params.year && req.params.year != 'null' && req.params.hn && req.params.hn != 'null')) {
        let readFile = await fs.readFileSync('./data/patient.json', 'utf8');
        if (readFile) {
            readJSON = await JSON.parse(readFile.toString());
            let params = req.params;
            let Found = await readJSON.filter((read) => {
                if(params.pid && req.params.pid != 'null'){
                    return read.pid == params.pid;
                }else{
                    return read.hn == `${params.hn}/${params.year}`;
                }
            });
            await res.status(200).send(Found);
            return true;
        }else{
            await res.status(500).send();
            return true;
        }
    }else{        
        res.status(404).send();
        return false;
    }
});

module.exports = router;