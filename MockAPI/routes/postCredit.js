const router = require('express').Router();
var dateFormat = require('dateformat');
const fs = require('fs');

router.get('/creditCode/:code?', async (req, res) => {
    responsecode['credit'] = req.params.code||200;    
    res.status(200).send({success:true,currentcode:responsecode['credit']});
    return true;
});

router.post('/credit/', async (req, res) => {
    let rescode = responsecode['credit'];
    let responseData = {rescode:rescode};
    
    if (rescode == 200){ //Fixed 200 With Condition(200/403)        
        if(req.body.p_run_hn && req.body.p_year_hn && req.body.p_credit_id && req.body.p_user_created){ //case 200
            let readFile = await fs.readFileSync('./data/credit_master.json', 'utf8');
            if (readFile) { // Read File Success
                readJSON = await JSON.parse(readFile.toString());
                let params = req.params;
                let Found = await readJSON.filter((read) => {
                    return read.hn == `${req.body.p_run_hn}/${req.body.p_year_hn}` && read.policyHolderId == req.body.p_credit_id;
                });
                if(Found[0]){ // Found Data
                    responseData = {
                        "pid": Found[0].pid,
                        "hn": Found[0].hn,
                        "periodStart": Found[0].periodStart,
                        "periodEnd": Found[0].periodEnd,
                        "policyHolderId": Found[0].policyHolderId,
                        "policyHolderName": Found[0].policyHolderName,
                        "creditNo": 16513873
                    };
                }else{ // Not Found Data
                    rescode = 403;
                    responseData = {
                        "status": "error"
                    };
                }
            }else{ // Read File Failed
                rescode = 500;
                responseData = {
                    "status": "error"
                };
            }
        }else{ //case 403
            rescode = 403;
            if(!req.body.p_run_hn && !req.body.p_year_hn){ //No Full HN                
                responseData = {
                    "ERROR-MESSAGE": "ORA-20999: -20999,ORA-20999: ต้องระบุ HN ในการเปิดสิทธิ\nORA-06512: at \"KAMOLJIT.PMKHMS\", line 889\nORA-06512: at \"KAMOLJIT.F_OPEN_CREDIT\", line 369",
                    "status": "error"
                };
            }else if(!req.body.p_run_hn || !req.body.p_year_hn){ //No HN || Year                
                responseData = {
                    "ERROR-MESSAGE": "ORA-20999: -20999,ORA-20999: HN ต้องระบุข้อมูลให้ครบ\nORA-06512: at \"KAMOLJIT.PMKHMS\", line 889\nORA-06512: at \"KAMOLJIT.F_OPEN_CREDIT\", line 369",
                    "status": "error"
                };
            }else if(!req.body.p_credit_id){ //No CreditID                
                responseData = {
                    "ERROR-MESSAGE": "ORA-20999: -20999,ORA-20999: ต้องระบุรหัสสิทธิที่จะเปิดด้วย\nORA-06512: at \"KAMOLJIT.PMKHMS\", line 889\nORA-06512: at \"KAMOLJIT.F_OPEN_CREDIT\", line 369",
                    "status": "error"
                };
            }else if(!req.body.p_user_created){ //No User                
                responseData = {
                    "ERROR-MESSAGE": "ORA-20999: -20999,ORA-20999: ต้องระบุ เจ้าหน้าที่ที่เกี่ยวข้องในการเปิดสิทธิ\tด้วย\nORA-06512: at \"KAMOLJIT.PMKHMS\", line 889\nORA-06512: at \"KAMOLJIT.F_OPEN_CREDIT\", line 369",
                    "status": "error"
                };
            }
        }
    }else if ( rescode == 403 ){ //Fixed 403 Duplicate Case        
        rescode = 403;
        responseData = {
            "ERROR-MESSAGE": "ORA-20999: -20999,ORA-20999: ท่านมีการเปิดสิทธิ จ่ายตรงกรมบัญชีกลาง (ผู้ป่วยนอก) ในวันนี้แล้ว ถ้าต้องการเปิดสิทธิอีก กรุณาติดต่อเจ้าหน้าที่ทะเบียน\nORA-06512: at \"KAMOLJIT.PMKHMS\", line 889\nORA-06512: at \"KAMOLJIT.F_OPEN_CREDIT\", line 369",
            "status": "error"
        };
    }
    res.status(rescode).send(responseData);
    return true;
});

module.exports = router;