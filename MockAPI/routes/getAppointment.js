const router = require('express').Router();
var dateFormat = require('dateformat');
const fs = require('fs');

router.get('/appointment/:date/:ignore?/:year/:hn', async (req, res) => {
    if (req.params.date && req.params.year && req.params.hn) {
        let readFile = await fs.readFileSync('./data/appointment.json', 'utf8');
        if (readFile) {
            readJSON = await JSON.parse(readFile.toString());
            let params = req.params;
            let Found = await readJSON.filter((read) => {
                return read.date == params.date && read.hn == `${params.hn}/${params.year}`;
            });
            await res.status(200).send(Found);
            return true;
        }else{
            await res.status(500).send();
            return true;
        }
    }else{        
        res.status(404).send();
        return false;
    }
});

module.exports = router;