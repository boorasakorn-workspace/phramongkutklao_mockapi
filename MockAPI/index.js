const express = require('express');
const app = express();
const server = require('http').Server(app);
const bodyParser = require('body-parser');
const port = process.env.PORT || 4001;
const routesPath = `${__dirname}/routes`;
const { Pool }  = require('pg');

global.pool = new Pool({"user": "postgres","host": "","database": "","password": "","port": ,"dateStrings":true});
global.responsecode = new Array();//Response HTTP Status Code
responsecode['visit'] = 200;
responsecode['credit'] = 200;

server.listen(port);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use((req, res, next) => {
    res.set({
        
    });
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader(
      "Access-Control-Allow-Headers",
      "*"
    );
    res.setHeader(
      "Access-Control-Allow-Methods",
      "GET, POST, PATCH, DELETE, OPTIONS"
    );
    next();
});

app.use('/ords/pmkords/hlab', require(`${routesPath}/getAppointment`));
app.use('/ords/pmkords/hlab', require(`${routesPath}/getCurrentCredit`));
app.use('/ords/pmkords/hlab', require(`${routesPath}/getCreditMaster`));
app.use('/ords/pmkords/hlab', require(`${routesPath}/getPatient`));
app.use('/ords/pmkords/hlab', require(`${routesPath}/postVisit`));
app.use('/ords/pmkords/hlab', require(`${routesPath}/postCredit`));